package events;

import java.util.Calendar;

/**
 * Created by youtous on 14/09/2016.
 */
public class Movie extends Event {
    private String movieTitle;


    public Movie(Calendar date, String movieTitle) {
        super(date);
        this.movieTitle = movieTitle;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    @Override
    public String getDescription() {
        return "Movie : " + this.movieTitle + " the " + this.date.toString();
    }
}
