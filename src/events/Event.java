package events;

import java.util.Calendar;

public abstract class Event {
	protected Calendar date;

	public Event(Calendar date) {
		super();
		this.date = date;
	}


	public abstract String getDescription();

	public Event() {
		super();
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Event [date=" + this.date.toInstant() + "]"
				+ this.getDescription();
	}

}