package events;

import java.util.Calendar;

import contacts.Contact;

public class Appointment extends Event {
	private Contact contact; // personne avec qui le rendez-vous a lieu
	private String place;    // où a lieu le rendez-vous

	public Appointment(Calendar date, Contact contact, String place) {
		super(date);
		this.contact = contact;
		this.place = place;
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	@Override
	public String getDescription() {
		return "Appointment with " + this.contact + " at " + this.place + ".";
	}


	
}
