package tp1;

import contacts.Contact;
import events.Appointment;
import events.Event;
import events.Movie;
import filters.Filter;
import organizer.*;

import java.util.Calendar;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        System.out.println("Tout cela ne vaut pas BlueJ");
        String test = "bonjour";
        System.out.println(
                test.substring(2, 4 + 1)
        );

        Contact p1 = new Contact("nom", "prénom");

        System.out.println(p1);

        /** Person methods test 1 */
        String name = "ARTHUR";
        p1.setLastName(name);

        String prenom = "Jerôme";

        p1.setFirstName(prenom);

        System.out.println(p1);

        System.out.println("Nom : " + p1.getLastName());
        System.out.println("Prénom : " + p1.getFirstName());

        /** Person methods test 2 */
        Contact p2 = new Contact("ROBERT", "Rémi");
        System.out.println("Deux personnes :" + "\n" +
                p1 + "\n" +
                p2
        );

        Calendar date = Calendar.getInstance();
        date.set(2010, 3, 19);


        Event rdv1 = new Appointment(date, p2, "Paris");

        System.out.println("Rendez-vous affiché :" + rdv1);

        System.out.println("Rendez-vous affiché lool :" + rdv1);

		/*Dans la méthode main, appelez la méthode applyFilter précédemment écrite en
        lui passant en paramètre un filtre créé à la volée à l'aide d'une classe anonyme
		et permettant de retourner uniquement les évènement de type Movie.*/

        // Filling the cal
        Organizer organizer = new Organizer();

        for (int i = 0; i < 100; i++) {
            Calendar dateN = Calendar.getInstance();
            dateN.set(2010, 3,
                    (int) (Math.random() * 29 + 1)
            );

            double rand = Math.random();
            if (rand >= 0.5) {
                organizer.insertEvent(
                        new Appointment(
                                dateN,
                                new Contact("Jerome", "lol"),
                                "Paris")
                );
            } else {

                organizer.insertEvent(
                        new Movie(
                                dateN,
                                "Un super film" + "n°" + i)
                );
            }
        }
        System.out.println(
                "Generated organizer :\n" +
                        organizer
        );

        Filter movieFilter = new Filter() {
            @Override
            public boolean accept(Event event) {
                return event.getClass() == Movie.class;
            }
        };

        List<Event> afterFilter = organizer.applyFilter(
                movieFilter
        );

        System.out.println(
                "Movies in organizer :\n" +
                        afterFilter
        );

        List<Event> afterFilter2 = organizer.applyFilter(
                event -> event.getClass() == Movie.class
        );

        System.out.println(
                "Movies in organizer :\n" +
                        afterFilter
        );
    }

}
