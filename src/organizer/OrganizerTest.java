package organizer;

import contacts.Contact;
import events.Appointment;
import events.Event;
import filters.DateFilter;
import org.junit.Test;

import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by youtous on 14/09/2016.
 */
public class OrganizerTest {
    @Test
    public void applyFilter() throws Exception {

        // Filling the cal
        Organizer organizer = new Organizer();

        for (int i = 0; i < 100; i++) {
            Calendar date = Calendar.getInstance();
            date.set(2010, 3,
                    (int) (Math.random() * 29 + 1)
            );

            organizer.insertEvent(
                    new Appointment(
                            date,
                            new Contact("Jerome", "lol"),
                            "Paris")
            );
        }
        System.out.println(
                "Generated organizer :\n" +
                organizer
        );

        Calendar before10Days = Calendar.getInstance();
        before10Days.set(2010, 3, 10);

        Calendar after10Days = Calendar.getInstance();
        after10Days.set(2010, 3, 20);

        DateFilter dateFilter = new DateFilter(before10Days, after10Days);

        List<Event> afterFilter = organizer.applyFilter(dateFilter);

        for (Event ev: afterFilter
             ) {
            assertTrue("Filtre défaillant",
                    ev.getDate().compareTo(before10Days) >= 0 &&
                            ev.getDate().compareTo(after10Days) <= 0
            );
        }

        System.out.println(
                "Filtered organizer :\n" +
                        afterFilter
        );

    }

    @org.junit.Test
    public void insertEvent() throws Exception {
        // Same date
        Calendar date = Calendar.getInstance();

        date.set(2010, 3, 19);
        Event eventReference = new Appointment(
                date,
                new Contact("Jerome", "lol"),
                "Paris reference"
        );

        // Organizer :
        Organizer organizer = new Organizer();

        assertTrue("Ajout d'un premier évènement", organizer.insertEvent(eventReference));

        Calendar dateAfter = Calendar.getInstance();
        dateAfter.set(2010, 3, 19 + 1);
        assertTrue("Ajout d'un évènement après",
                organizer.insertEvent(
                        new Appointment(
                                dateAfter,
                                new Contact("Jerome", "lol"),
                                "Paris après"
                        )
                )
        );


        Calendar dateBefore = Calendar.getInstance();
        dateBefore.set(2010, 3, 19 - 1);
        assertTrue("Ajout d'un évènement après",
                organizer.insertEvent(
                        new Appointment(
                                dateBefore,
                                new Contact("Jerome", "lol"),
                                "Pari avant"
                        )
                )
        );

        assertFalse("Ajout d'un évènement déjà existant",
                organizer.insertEvent(
                        new Appointment(
                                dateBefore,
                                new Contact("Jerome", "lol"),
                                "Paris"
                        )
                ));

        System.out.println(organizer);
    }
}