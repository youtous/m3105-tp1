package organizer;

import events.Event;
import events.EventComparator;
import filters.Filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by youtous on 14/09/2016.
 */
public class Organizer {
    // liste d'évènements ordonnés par dates croissantes
    private ArrayList<Event> events = new ArrayList<Event>();

    /**
     * insère un évènement au bon endroit dans la liste
     *
     * @param evt évènement à insérer
     * @return true si l'insertion a réussi, false sinon (un évènement pour la même date est déjà présent dans la liste)
     */
    public boolean insertEvent(Event evt) {
        /*
        * The list must be sorted !
        * */
        int pos = Collections.binarySearch(this.events, evt, new EventComparator());
        if (pos < 0) {
            this.events.add((-(pos) - 1), evt);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return this.events.toString();
    }

    /**
     * crée et retourne une liste contenant uniquement les évènements acceptés par filters
     * @param filter filtre à appliquer
     * @return liste d'évènements répondant au critère spécifié par filters
     */
    public List<Event> applyFilter(Filter filter) {
        List<Event> acceptedEvents = new ArrayList<Event>();

        for (Event ev: this.events
             ) {
            if(filter.accept(ev)) {
                acceptedEvents.add(ev);
            }
        }
        return acceptedEvents;
    }
}
