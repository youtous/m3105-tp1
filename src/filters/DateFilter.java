package filters;

import events.Event;

import java.util.Calendar;

/**
 * Created by youtous on 14/09/2016.
 */
public class DateFilter implements Filter {
    private Calendar start, end;

    public DateFilter(Calendar start, Calendar end) {
        this.start = start;
        this.end = end;
    }

    /**
     * filtre un évènement en fonction de sa date
     *
     * @param event évènement à filtrer
     * @return true si la date de l'évènement est comprise entre les dates start et end (incluses), false sinon
     */
    @Override
    public boolean accept(Event event) {
        return this.start.compareTo(event.getDate()) <= 0 && this.end.compareTo(event.getDate()) >= 0;
    }

}
