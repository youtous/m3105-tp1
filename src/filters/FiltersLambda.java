package filters;

/**
 * Created by youtous on 16/09/2016.
 */
public class FiltersLambda {


    public static Filter and(Filter filter, Filter otherFilter) {
      return event -> (filter.accept(event) && otherFilter.accept(event));
    }

    public static Filter or(Filter filter, Filter otherFilter) {
        return event -> (filter.accept(event) || otherFilter.accept(event));
    }

    public static Filter not(Filter filter, Filter otherFilter) {
        return event -> (!filter.accept(event) && !otherFilter.accept(event));
    }

}
