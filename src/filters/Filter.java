package filters;

import events.Event;

/**
 * Created by youtous on 14/09/2016.
 */
public interface Filter {
    boolean accept(Event event);
}
