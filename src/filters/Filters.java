package filters;

import events.Event;

/**
 * Created by youtous on 16/09/2016.
 */
public class Filters {

    public static Filter and(Filter filter, Filter otherFilter) {
        return new Filter() {
            @Override
            public boolean accept(Event event) {
                return filter.accept(event) && otherFilter.accept(event);
            }
        };
    }

    public static Filter or(Filter filter, Filter otherFilter) {
        return new Filter() {
            @Override
            public boolean accept(Event event) {
                return filter.accept(event) || otherFilter.accept(event);
            }
        };
    }

    public static Filter not(Filter filter, Filter otherFilter) {
        return new Filter() {
            @Override
            public boolean accept(Event event) {
                return !filter.accept(event) && !otherFilter.accept(event);
            }
        };
    }

}
