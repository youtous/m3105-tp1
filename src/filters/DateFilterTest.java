package filters;

import contacts.Contact;
import org.junit.Test;
import events.Appointment;

import java.util.Calendar;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by youtous on 14/09/2016.
 */
public class DateFilterTest {
    @Test
    public void accept() throws Exception {
        Calendar before2Days = Calendar.getInstance();
        before2Days.set(2010, 3, 19 - 2);

        Calendar after2Days = Calendar.getInstance();
        after2Days.set(2010, 3, 19 + 2);

        DateFilter dateFilter = new DateFilter(before2Days, after2Days);


        Calendar before1Day = Calendar.getInstance();
        before1Day.set(2010, 3, 19 - 1);

        assertTrue("Dans l'interval j-1", dateFilter.accept(
                new Appointment(
                        before1Day,
                        new Contact("Jerome", "lol"),
                        "Paris"
                )
        ));


        Calendar after1Day = Calendar.getInstance();
        after1Day.set(2010, 3, 19 + 1);

        assertTrue("Dans l'interval j+1", dateFilter.accept(
                new Appointment(
                        after1Day,
                        new Contact("Jerome", "lol"),
                        "Paris"
                )
        ));

        assertTrue("Sur la borne supérieure de l'intervalle", dateFilter.accept(
                new Appointment(
                        after2Days,
                        new Contact("Jerome", "lol"),
                        "Paris"
                )
        ));

        assertTrue("Sur la borne inférieure de l'intervalle", dateFilter.accept(
                new Appointment(
                        before2Days,
                        new Contact("Jerome", "lol"),
                        "Paris"
                )
        ));

        Calendar after3Days = Calendar.getInstance();
        after3Days.set(2010, 3, 19 + 3);

        assertFalse("En dehors de la borne supérieure de l'intervalle", dateFilter.accept(
                new Appointment(
                        after3Days,
                        new Contact("Jerome", "lol"),
                        "Paris"
                )
        ));

        Calendar before3Days = Calendar.getInstance();
        before3Days.set(2010, 3, 19 - 3);

        assertFalse("En dehors de la borne inférieure de l'intervalle", dateFilter.accept(
                new Appointment(
                        before3Days,
                        new Contact("Jerome", "lol"),
                        "Paris"
                )
        ));
    }

}