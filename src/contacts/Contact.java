package contacts;

public class Contact {
	private String lastName;
	private String firstName;
	
	/**
	 * Constructeur à paramètres de Personne. Une Personne est définie par :
	 * @param nom - Le nom de la Personne
	 * @param prenom - Le prénom de la Personne
	 */
	public Contact(String nom, String prenom) {
		super();
		this.lastName = nom;
		this.firstName = prenom;
	}

	/**
	 * Accesseur du nom de la Personne
	 * @return String - le nom de la Personne
	 */
	public String getLastName() {
		return lastName;
	}
	
	/** Mutateur du nom de la Personne
	 * @param nom - le nom de la Personne
	 */
	public void setLastName(String nom) {
		this.lastName = nom;
	}
	/**
	 * Accesseur du prénom de la Personne
	 * @return String - le prénom de la Personne
	 */
	public String getFirstName() {
		return firstName;
	}
	/** Mutateur du prénom de la Personne
	 * @param prenom - le prénom de la Personne
	 */
	public void setFirstName(String prenom) {
		this.firstName = prenom;
	}

	/* Affichage sous forme d'une chaîne de caractères des caractéristiques d'une personne.
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Personne [nom=" + lastName + ", prenom=" + firstName + "]";
	}
	
}
