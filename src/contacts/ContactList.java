package contacts;

import java.util.ArrayList;

public class ContactList {

	private ArrayList<Contact> contacts = new ArrayList<>();

	public int size() {
		return contacts.size();
	}

	public Contact get(int index) {
		return contacts.get(index);
	}

	public boolean add(Contact e) {
		return contacts.add(e);
	}

	public void add(int index, Contact element) {
		contacts.add(index, element);
	}
	
    public String getFirstName(String lastName) {
        Contact foundContact = null;
        // Code extrait et remplacé par la méthode extraite
        foundContact = getContactByName(lastName, foundContact);
        
        if (foundContact == null)
            return null;
        
        return foundContact.getFirstName();    
    }

	private Contact getContactByName(String lastName, Contact foundContact) {
		ArrayList<Contact> contactsCopy = contacts;
		for (int i = 0; i < contactsCopy.size() && foundContact == null; i++) {
            Contact c = contactsCopy.get(i);
            if (lastName.equals(c.getLastName())) {
                foundContact = c;
            }
        }
		return foundContact;
	}
}
